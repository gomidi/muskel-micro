## MuSkeL Plugin

The MuSkeL plugin provides some extra niceties for using micro with
the MuSkeL language. The main thing this plugin does is
run `muskel fmt` for you automatically. 
The plugin also provides `muskel smf` functionality.

Press `CTRL+B` (the default binding) to run `muskel smf`. You
can rebind this in your `bindings.json` file with the action `muskel.smf`.
