# MuSkeL Plugin for Micro

This repository holds the MuSkeL plugin for micro 2.0.

This plugin will let you automatically run `muskel fmt` on save.

Use `CTRL+B` to run `muskel smf`.

Install by cloning this into your plug directory:

```
git clone https://gitlab.com/gomidi/muskel-micro ~/.config/micro/plug/muskel
```

Then copy the `muskel.yaml` file to `~/.config/micro/syntax/`.