VERSION = "0.0.1"

local micro = import("micro")
local config = import("micro/config")
local shell = import("micro/shell")
local buffer = import("micro/buffer")

function init()
    config.MakeCommand("muskelsmf", muskelsmf, config.FileComplete)
    config.MakeCommand("muskelfmt", muskelfmt, config.FileComplete)

    config.AddRuntimeFile("mskl", config.RTHelp, "help/muskel-plugin.md")
    config.TryBindKey("CtrlB", "lua:muskel.muskelsmf", false)
end

function onSave(bp)
    micro.InfoBar():Error(bp.Buf:FileType())
    if bp.Buf:FileType() == "mskl" then
        muskelfmt(bp)
    end
    return true
end

function muskelfmt(bp)
    bp:Save()
    local cmdargs = {"--fmt", bp.Buf.Path}
    shell.JobSpawn("muskel", cmdargs, nil, muskelStderr, muskelExit, bp)
end

function muskelStderr(err)
    micro.TermMessage(err)
end

function muskelExit(output, args)
    local bp = args[1]
    bp.Buf:ReOpen()
end

function muskelsmf(bp)
    bp:Save()
    local cmdargs = {"smf", "--fmt", bp.Buf.Path}
    shell.JobSpawn("muskel", cmdargs, nil, muskelStderr, muskelExit, bp)
end
